---
layout: post
title:  "Welcome to C++!"
date:   2019-05-27 10:37:14
categories: c++
---

## Hello World, The First Cpp Program
The following code block shows a simple C++ program.
```cpp
//
#include <iostream>
//COMMENTS
// this is a single line comment
/*this is 
 a multi-line
 comment
*/

// The following line defines a function, with int type return value and void type parameters
int main (void)
{
  std::cout << "Hello World\n"; // this is a statement
}
```
The the above code will output the following result,
```console
Hello World
```
The code needs to be compiled before we can execute it. 
Compiling a C++ program depends the operating system and compiler. Generally, it involves the following steps,
1. C++ **preprocessor** ---> *preprocessor directives*(e.g., ***#include***)
1. C++ **Compiler** -- translates --> *program code* --> native (assembly or machine) *object code*
1. **Linker** links *object code* to *library modules*

## Memory
- **binary digit**: 0 or 1
- **bits**: stores a binary digit
- **bytes**: a group of 8 consequtive bits
- **RAM**: random access memory, a contiguous sequence of bits
- **address**: bytes are sequentially addressed 

![avatar](https://gitlab.com/BoltzmannBlitz/cpp-daily/raw/master/_includes/pics/ram.png "Bits and bytes in memory.")

![avatar](https://gitlab.com/BoltzmannBlitz/cpp-daily/raw/master/_includes/pics/int_in_ram.png "Representation of an integer in memory.")

## Types and Variables
A variable is a symbolic name for a memory location in which data can be stored and subsequently recalled. All variables have two attributes in C++.
- A **type** is established when the variable is defined (e.g., integer, real, character). Once defined a variable in C++, its type cannot be changed.
- A **value** can be changed by assigning a new value to the variable.

## Integer Numbers
The following table shows some integer types, their sizes and ranges.

| Integer type &nbsp;&nbsp;&nbsp; | Size &nbsp;&nbsp;&nbsp; | Range &nbsp;&nbsp;&nbsp; |
| :-------------  | :-----------: | ------------: |
| short           | 2 Bytes       | −128 ~ 127    |
| int             | 2 Bytes       | −128 ~ 127    |
| long            | 4 Bytes       | -32768 ~32767 |
| unsigned short  | 2 Bytes       |    0 ~ 255    |
| unsigned int    | 2 Bytes       |    0 ~ 255    |
| unsigned long   | 4 Bytes       |    0 ~ 65535  |

- a **literal integer** is an *int* type
- a *long* type has an *L* or *l* suffix
- an *unsigned* type has a *U* or *u* suffix

[C++ Fundamental types](https://en.cppreference.com/w/cpp/language/types)

Integer numbers can be represented in decimal, octal (start with 0), or hexadecimal form (start with 0x).
- decimal 42 
- octal   052
- hexadecimal 0x2A

Some examples of calculation among binary, decimal, octal, and hexadecimal.
- binary to decimal: $1010011 = 1 \times 2^6 + 1 \times 2^4 + 1 \times 2^1 + 1 \times 2^0 = 64 + 16 + 2 + 1 = 83$
- binary to octal:  $1010011 --> 0(001)(010)(011) = 0123$
- binary to hexadecimal: $1010011 --> 0101\;0011 = 0\mathrm{x}(1 \times 2^2 + 1 \times 2^0)(1 \times 2^1 + 1 \times 2^0) = 0\mathrm{x}53$
- octal to decimal: $ 0123 = 1 \times 8^2 + 2 \times 8^1 + 3 \times 8^0 = 64 + 16 + 3 = 83$
- hexadecimal to decimal: $ 0\mathrm{x}53 = 5 \times 16^1 + 3 \times 16 = 80 + 3 = 83$
- decimal to binary: $(83/2-->1)(41/2-->1)(20/2-->0)(10/2-->0)(5/2-->1)(2/2-->0) --> 1010011$

## Real Numbers
- *float* uses 4 bytes, e.g., 0.06f, 0.06F
- *double* uses 8 bytes. A *long double* uses more bytes for better accuracy, e.g., 3.141592654L, 3.141592654l.
A literal real is aways assumed as *double* type. Scientific notations are also used, e.g., 3.14159e-2, -3.14159E-2.

## Characters and Strings
A character variable is defined as *char* and consumes 2 bytes. Character coding system (e.g., ASCII). The following code shows two ways to define character variables that holds *A*.
```cpp
char ch1 = 'A'; // a literal character is written by enclosing the character between a pair of single quotes
char ch2 = 65; // the character A in ASCII has code 65.
```
Literal characters may also be specified using their numeric code value with the escape sequence ```'\ooo'``` (i.e., a backslash followed by up to three octal digits). For example (in ASCII),
'\012' // new line, decimal code 10
'\101' // 'A', decimal code 65
'\0'   // null, decimal code 0

A string variable is defined as the type a *pointer* to charactor *char\**.
```cpp
// A literal string is written by enclosing characters between a pair of double quotes.
char *str = "Hello World" 

// With backslash, you can write a long string that is beyond a single line.
char *long_str = "This is \
                  a long string \
                  in multiple lines."
```
Strings are always ended with a null character ('\0') which is added at the end of the string by the compiler.
**Escape notations** can be used to represent nonprintable characters, e.g.,
- '\n'   //new line
- '\t'   // horizontal table
- '\''   // single quote (')
- '\"'   // double quote (")
- '\101' //'A'

## Names and Key Words
Names (**identifiers**) include variable names, function names, type names, and marco names.
Names should consist of one or more characters, including letters ('A'-'Z', 'a'-'z'), digits ('0'-'9'), and under score ('\_'), but the first character should not be a digit.
```cpp
//some valid identifiers
int land_use;
int land_use2;
int _internal_land_use;
int Land_use; // Note that land_use and Land_use are two different variables.
// wrong identifiers such as 
// int 2land_use;
```
[**Key words**](https://en.cppreference.com/w/cpp/keyword)

## Type Conversion
**Implicit Type Conversion**, also known as 'automatic type conversion' is done by the compiler.
C style **Type Cast** is like `TypeName b = (TypeName)a`.
The following code shows a simple transformer takes decimal latitude and longitude values and gives out their degree/minitue/second format. 
```cpp
#include <iostream>

int main(void)
{
  int d_lat, m_lat, s_lat, d_lon, m_lon, s_lon;
  float latitude, longitude;

  //using std::cout to output the string into console
  std::cout << "Give a latitude value and a longitude value:";
  //using std::cin to get input values from keyboard input
  std::cin >> latitude >> longitude;

  // transform latitude into dms format
  d_lat = (int)latitude;   //explicitly convert a float into a integer
  d_lon = longitude;  //implicitly convert a float into a integer

  m_lat = 60 * (latitude - d_lat);
  m_lon = 60 * (longitude - d_lon);

  s_lat = 60 * (60 * (latitude - d_lat) - m_lat);
  s_lon = 60 * (60 * (longitude - d_lon) - m_lon);

  std::cout << "Transforms "<<latitude<<" and "<<longitude<<" as "<<std::endl;
  std::cout <<d_lat<<'D'<<m_lat<<'M'<<s_lat<<'S' << " and "<<
    d_lon<<'D'<<m_lon<<'M'<<s_lon<<'S' << std::endl;
}
```
Run the above code and give the latitude and longitude values as 123.35355312 and 42.2353253. The result is shown as the following.
```console
Give a latitude value and a longitude value:123.35355312
42.2353253
Transforms 123.354 and 42.2353 as 
123D21M12S and 42D14M7S
```




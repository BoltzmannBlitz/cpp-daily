---
layout: post
title:  "Functions"
date:   2019-05-30 16:22:14
categories: c++
---  
  
## The return Statement  

The *return* statement enables a function to return a value to its caller. It has the general form,
  
return *expression*;  
  
where *expression* denotes the value returned by the function. The type of this value should match the return type of the function. If a function whose return type is *void* may have the following return statement.
  
return;  
  
For example,

```cpp
int main (void)
{
  cout << "Hello World\n";
  return 0;
}
```  
  
## Function Basics
- **Function definition**: interface and body
	- **Interface** (**prototype**): 
		- **name**, a unique identifier
		- **parameters**, function parameters (**signature**) is a set of zero or more typed identifiers used for passing values to and from the function
		- **return type**, the type of the value the function returns, the function returns nothing should have the return type *void* 
	- **Body**, a set of statements define the computational steps of the function.

Using a function involves "calling" it. A **function call** consists of the function name followed by the call operator brackets '()', inside which zero or more comma-sperated **arguments** appear. 
  
Here we define a *Power* function to calculate *exponent* power of a given *base*, before we use the function we need to **declare** it. A function declaration consists of the *function name*, *parameter type*, and *return type*. Parameter names may be omitted, but not recommended, unless the role of the parameters is obvious.  

```cpp
// this is a functino declaration
int Power(int base, unsigned int exponent);
```
  
The function defines as the following code, 
```cpp
int Power (int base, unsigned int exponent)
{ // function body goes here
  int result = 1;

  for (int i = 0; i < exponent; ++i)
  {
  	result *= base;
  }
  return result;
}
```
We can call the *Power* function in the *main* function.
```cpp
#include <iostream>

main (void)
{
  cout << "2^8 = " << Power(2, 8) << '\n';
}
```
  
## Parameters and Arguments
  
- **value parameter** receives a *copy* of the value of the argument passed to it. If the function makes any changes to the parameter, will not affect the argument.
- **reference parameter** receives the argument passed to it and works on it directly. Any changes made by the function to a reference parameter is in effect directly applied to the argument.  
  
In the context of function calls, the two types of passing arguments are respectively called
- **pass-by-value**, and
- **pass-by-reference**.
  
## Global and Local Scope
**Global scope** indicates everything that defined at the program level (outside functions, classes, or other '{}' defined code blocks). Uninitialized global variables are automatically initialized to zero. Each block in a program defines a **local scope**.
  
The following defines three distinct scopes, each containing a distinct *some_variable*.
```cpp
int some_variable;  // some_variable is global
void Fun(int some_variable)  // some_variable is local to this body of Fun
{
  if (some_variable > 0)
  {
  	double some_variable;  // here some_variable is local to this block
  }
}
```
  
## Scope Operator 
The unary scope operator :: which takes a global entity as argument,

```cpp
int error;
void Error (int error)
{
  // ...
  if (::error != 0)
  {
  	// handle error ...
  }
}
```  
  
## Auto Variable
The lifetime of a local variable is limited and is determined automatically. These variables are also called **automatic**, the storage specifier *auto* explicitly specify a local variable to be automatic. This is rarely used because all local variables are by default automatic.

## Register Variables
The storage specifier *register* may be used to indicate the compiler that the variable should be stored in a register if possible. For frequently-used variables (e.g., loop variables), efficiency gains can be obtained by keeping the variable in a register instead thereby avoiding memory access for that variable. Note that *register* only provides a hint to the compiler, and the compiler may choose to use a register or not, even many optimizing compilers may guess and use *registers* where they are likely to improve the preformance of the program.  
```cpp
for (register int i = 0; i < n ; ++i) sum += 1;
```
  
## Static variables and Functions
  
The storage class specifier *static* is used to confine the accessibility of a global variable or function to a single file. The *static* specifier prevents them from accessing from outside the file. 

```cpp
static int base, exponent;

static int Power (int base, unsigned int exponent);
```  
A local variable with *static* specifier remains only accessible within its local scope, while its lifetime is no longer be confined to this scope, but will instead be global. In other words, a static local variable is a global variable which is only accessible within its local scope.

```cpp
void Error (char *message)
{
  static int count = 0;

  if (++count > limit)
  	Abort();
  // ... 
}  
```  
  
Static local variables are automatically initialized to 0.  
  
## Extern Variables and Functions
  
The *extern* declaration tells the compiler that the variable or function is defined elsewhere may be needed.
This is a **declaration** because it does not lead to any storage being allocated for the variable.  It is a poor programming practice to include an initializer for an ***extern*** variable, since this causes it to become a varialbe definition and have storage allocated for it. If there is another definition for the variable elsewhere, it will eventually classh with this one. The best place for ***extern*** declarations is usually in header file so that they can be easily included and shared by source files.  If a function prototype is declarated as extern, it has no effect when a prototype appears at the global scope, it is more useful to declare function prototypes inside a function.

```cpp
extern int size; //varialbe declaration
extern int size = 10; // no longer a declaration !

double Tangent (double angle)
{
  extern double sin(double);  // defined elsewhere
  extern double cos(double);  // defined elsewhere

  return sin(angle) / cos(angle);
}
```
  
## Symbolic Constants
Defining a variable with keyword ***const*** makes the variable read-only (i.e., a symbolic constant). A constant must be initialized to some value when it is defined. Once defined, the value of a constant cannot be changed.
  
```cpp
const int    maxIteration = 10000;
const double pi = 3.141592654;
const maxSize = 128; // a constant without type specifier is assumed to be of type int
```
  
Constant and Pointers  
- a **constant pointer**
- a **pointer points to a constant**
- a **constant pointer** points to a **constant**

```cpp
const char *str1 = "pointer to constant";
char *const str2 = "constant pointer";
const char *const str3 = "constant pointer to constant";
// the value of str1 should not be changed
// The following statement is illegal!
// str1[0] = 'A';

// str1 can be changed as denoting to another address
str1 = "ptr to const" // ok

// the pointer str2 should not denote another address.
// the following statement is illegal!
// str2 = "ptr to const"

// alter the value of the variable that str2 denotes is ok
str2[0] = 'P';

// both the address str3 denotes and the value cannot be changed
// str3 = "ptr to const";
// str3[0] = 'P';
```
  
A function parameter may also be declared to be constant, indicating that the function does not change the value of the parameter.
```cpp
int Power (const int base, const int exponent)
{
  //...
}
```
  
A function may also return a constant result:
```cpp
const char* SystemVersion (void)
{
  return "5.2.1";
}
```

The usual place for constant definition is within header files so that they can be shared by source file.
  
## Enumerations
An enumeration of symbolic constants is introduced by an ***enum*** declaration. This is useful for declaring a set of closely-related constant. The following example defines four **enumerators** which have integral values starting from 0 (i.e., *north* is 0, *south* is 1, *east* is 2, *west* is 3).
```cpp
enum {north, south, east, west};
```

The default numbering of enumerators can be overruled by explict initialization,
```cpp
enum {north = 10, south, east = 0, west} 
```  
  
where *south* is 11, *west* is 1.
  
A named enumerator becomes a user-defined type.
```cpp
enum Direction { north, south, east, west}
Direction d;

//...
switch (d)
{
  case north: // ...
  case south: // ...
  case east:  // ...
  case west:  // ...
}
```
  
```cpp
enum Bool {false, true};
```

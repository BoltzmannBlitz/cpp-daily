---
layout: post
title:  "Array, Pointers, and References"
date:   2019-06-04 11:07:01
categories: c++
---  

- ***array*** consists of a set of **elements** which are of the same type and stored continously in memory. Array itself has a symbolic name, not its elements. Each element is identified by an **index**, which denotes the position of the element in the array. The number of elements in an array is called its dimension.
- ***pointer*** is the address of an object in memory. The act of getting to an object via a pointer to it, called **dereferencing** the pointer. Pointer variables are defined to point to objects of a specific type so that when the pointer is dereferenced, a typed object is obtained. Pointers are useful for creating dynamic objects during program execution. Normal global and local objects which are allocated storage on the runtime stack. Dynamic objects are allocated memory from the storage area called **heap**. The scope of dynamic objects is controlled by the programmer.
- ***reference*** provides an alternative symbolic name (**alias**) for an object. Acessing an object through a reference is exactly the same as accessing it through its original name. They are used to support the call-by-reference style of function parameters, especially when large objects are being passed to function.

## Arrays##
```cpp
int time_zone[24]; // defines an array named as time_zone of 24 int elements

int time_zone[0] = 0;    // assign 0 to the first element of time_zone
int time_zone[23] = +23; // assign +23 to the last element of time_zone
```  

The individual elements of the array are accessed by indexing the array. The above statement defeines an array named *time_zone* of 24 integers. The first array element always has the index 0. Therefore, *time_zone[0]* and *time_zone[23]* denote, respectively the first and the last element of *time_zone*. 
  
Attempting to access a nonexistent array element (e.g., *time_zone[-1]* or *time_zone[24]* ) leads to a serious runtime error ("index out of bounds error").
  
Processing of an array usually involves a loop which goes through the array element by element.
  
```cpp
const int size = 3;

double Average (int nums[size])
{
  double average = 0;

  for (register i = 0; i < size; ++i)
  {
  	average += nums[i];
  }
  return average/size;
}
```  

Initialize an array,
```cpp
int nums[3] = {3, 6, 9};
int nums[3] = {3, 6};   // nums[2] is initialized to 0
int nums[] = {3, 6, 9}; // equals to "int nums[3] = {3, 6, 9};"
```  
  
A C++ string is an array of characters. However, string has one more null character at the end of the array.
```cpp
char str[] = "HELLO WORLD"; // defines a string of 12 elements, the last element is a null character
char str[] = {'H', 'E', 'L', 'L', 'O', ' ', 'W', 'O', 'R', 'L', 'D'} // a char array of 11 elements
```  
  
The dimesion of an array can be calculated simply by the following, where ar is the *array* and *Type* is the element type
***sizeof(ar)/sizeof(Type)***  
  
## Multidimensional Arrays##
  
An array may have more than one dimension, but its organization in memory is the same as a contiguous sequence of elements. The following table can be represented by a 2-dimensional array.
  
|          | &nbsp;&nbsp; Spring &nbsp;&nbsp; | &nbsp;&nbsp; Summer &nbsp;&nbsp; | &nbsp;&nbsp; Autumn &nbsp;&nbsp; | &nbsp;&nbsp; Winter &nbsp;&nbsp; |
| :--------------------------------:          | :----: | :----: | :----: | :----: | 
|&nbsp;&nbsp;Sydney&nbsp;&nbsp;&nbsp;&nbsp;   | 26     | 34     | 22     | 17     |
|&nbsp;&nbsp;Melbourne&nbsp;&nbsp;&nbsp;&nbsp;| 24     | 32     | 19     | 13     |
|&nbsp;&nbsp;Brisbane&nbsp;&nbsp;&nbsp;&nbsp; | 28     | 38     | 25     | 20     |  
  
```cpp
int season_temp[3][4];
```
  
The organization of this array in memory is as 12 consecutive integer elements. Accessing the array by specifying the index of each dimension. For example *season_temp[0][3]* retrieves the winter temperature of Sydney. The array may be initialized using a nested initializer,
```cpp
int season_temp[3][4] = 
  {
  	{26, 34, 22, 17},
  	{24, 32, 19, 13},
  	{28, 38, 25, 20}
  };
```
  
It is equivalent to
```cpp
int season_temp[3][4] = {
  26, 34, 22, 17, 24, 32, 19, 13, 28, 38, 25, 20};
```  
  
Multidimensional arrays cannot have unknown size in a dimension other than the first. Note that when initializing an multidimensional array, the compiler sequentially initializes each element of the array. Each brace indicates a sub-initializer. Taking the example *int season_temp[3][4] = 
{ {26}, {24}, {28} };*, the first left brace indicates an initializer to *season_temp[3][4]*. When the compiler goes to the second left brace, it has the first sub-initializer to the first sub-set *season_temp[0][4]*. Thus the first element of *season_temp[0][4]*, i.e., *season_temp[0][0]* is initialized as *26* while the rest of its elements are initialized as 0.

We may have the following initializing statement,
```cpp
// providing the first element of each row, rest elements of each row are initialized as 0
int season_temp[3][4] = 
  {
  	{26},
  	{24},
  	{28}
  };
```  
  
or  
```cpp
// the first dimension is omitted and inferred from the initializer as 3. 
int season_temp[][4] = 
  {
  	{26, 34, 22, 17},
  	{24, 32, 19, 13},
  	{28, 38, 25, 20}
  };
```  
  
Processing a multidimensional array is similar to a one-dimensional array, but uses nested loops instead of a single loop.  
```cpp
const int rows = 3;
const int columns = 4;

int season_temp[rows][columns] = 
{
  	{26, 34, 22, 17},
  	{24, 32, 19, 13},
  	{28, 38, 25, 20}  
};

int HighestTemp (int temp[rows][columns])
{
  int highest = -100;

  for (register i = 0; i < rows; ++i)
  	for (register j = 0; j < columns; ++j)
  	  if (temp[i][j] > highest)
  	  	highest = temp[i][j];
  return highest;
}
```  
  
## Pointers ##

A **pointer** is the *address* of a memory location and provides an indirect way of accessing data in memory. A pointer variable is defined to "point to" data of a specific type.
  
```cpp
int *ptr1;  // pointer to an int
char *ptr2; // pointer to a char
```
  
The *value* of a pointer variable is the address to which it points. In the following example, & is the **address** operator, it takes a variable as argument and returns the memory address of that variable.
  
```cpp
int num;
int *ptr1 = &num;  \\ the address of num is assigned to ptr1.
```   
  
Given that *ptr1* to get to num, the expression * *prt* dereferences *ptr1* to get to what it points to, and is therefore equvalent to *num*. The symbol * is the **dereference** operator; it takes a pointer as argument and returns the contents of the locatioin to which it points. In general, the type of a pointer must match the type of the data it is set to point to. A pointer of type *void\**, however, will match any type. This is useful for defining pointers which may point to data of different types, or whose type is originally unknown. A pointer may be **cast** (type converted) to another type. The following example converts *ptr1* to *char* pointer before assigning it to *ptr2*.  

```cpp
ptr2 = (char*) ptr1; 
```  
  
A pointer may be assigned to the value 0 (**null** pointer). The null pointer is used for initializing pointers, and for marking the end of pointer-based data structure (e.g., linked lists).  
  
## Dynamic Memory
In addition to the program stack (which is used for storing global variables and stack frames for function calls), another memory area, called the **heap**, is provided. The heap is used for dynamically allocating memory blocks during program execution. **Heap** is also called **dynamic memory**. The program stack is called **static memory**.

Two operators are used for allocating and deallocating memory blocks on the heap.
- ***new*** operator takes a type as argument and allocated a memory block for an object of that type.
- ***delete*** operator is usded for releasing memory blocks allocated by ***new***.  
  
```cpp
int  *ptr = new int;
char *str = new char[10];
// ...
delete ptr;
delete [] str;  // Note that when the block to be deleted is an array, an additional [ ] should be included to indicated this.
```
  
Memory allocated from the heap does not obey the same scope rules as normal variables. In the following example, the local variable *str* is destroyed, but the memory block pointed to by *str* is not. The latter remains allocated until explicitly released by the programmer. Delete an object (i.e., a variable) on the stack will cause the runtime error. It is harmless to apply delete to the 0 pointer. 
  
```cpp
void Foo (void)
{
  char *str = new char[10];
  // ...
  // the memory block in heap is not released until explicitly use delete
  // after the function returns, str is destroyed, 
  // if the memory block is not released, it can not be accessed any more.
  // this causes memory leak.
}
```
  
Dynamic objects are useful for creating data which last beyond the function call which creates them. The following example shows a function which takes a string parameter and return a *copy* of string.  

```cpp
#include <string>

char* CopyOf (const char *str)
{
  char *copy = new char[strlen(str) + 1];

  strcpy(copy, str);
  return copy;
}
```  
  
Because of the limited memory resources, there is always the possibility that dynamic memory may be exhausted during program execution, especially when many large blocks are allocated and none released. Should *new* be unable to allocated a block of the requested size, it will return 0 instead.It is the responsibility of the programmer to deal with sucn possibilities. The ***exception handling mechanism*** of C++ provides a practical method of dealing with such problems.
  
## Pointer Arithmetic##
In C++ one can add an integer quantity to or subtract an integer quantity from a pointer. This is called pointer arithmetric. Pointer arithmetic is ***not*** the same as integer arithmetic, because the outcome depends on the size of the object pointed to. For example, suppose that an *int* is represented by 4 bytes, Now given
```cpp
char *str   = "HELLO";
// *(++str) = 'E'
int  nums[] = {10, 20, 30, 40};
int  *ptr   = &nums[0];  // pointer to first element
// *(++ptr) = 20 i.e., nums[1]
```  
  
*str++* advances *str* by one *char* (i.e., one byte) so that it points to the second character of "HELLO", whereas *ptr++* advances *ptr* by one *int* (i.e., four bytes) so that it points to the second element of *nums*. The elements of "HELLO" can be referred to as *\*str*, *\*(str + 1)*,  *\*(str + 2)*, etc. Similarly, the elements of *nums* can be referred to as *\*ptr*, *\*(ptr + 1)*, *\*(ptr + 2)*, etc. Pointer subtraction happens between two pointers of the same type. Pointer arithmetic is useful when processing the elements of an array.
  
```cpp
int *ptr1 = &nums[1];
int *ptr2 = &nums[3];
int n = ptr2 - ptr1; // n becomes 2
```  
    
```cpp
void CopyString (char *dest, char *src)
{
  while (*dest++ = *src++)
    ;
}
```

## Function Pointers ##
It is possible to take the address of a function and store it in a function pointer. The pointer can then be used to indirectly call the function. The following code defines a function pointer named *Compare* which can hold the address of any function that takes two constant character pointers as arguments and returns an integer. The string comparison library function *strcmp*, for example, 
  
```cpp
int (*Compare) (const char*, const char*);

Compare = &strcmp;  // Compare points to strcmp function
// The & operator is not necessary and can be omitted.
// Compare = strcmp;
```  
  
Alternatively, the pointer can be defined and initialized at once:
```cpp
int (*Compare) (const char*, const char*) = strcmp;
```  
  
When a function address is assigned to a function pointer, the two types must match. The above definition is valid because *strcmp* has a matching function prototype,  
```cpp
int strcmp(const char*, const char*);
```  
  
Given the above definition of *Compare*, *strcmp* can be either called directly, or indirectly via *Compare*. The following three calls are equivalent,  
```cpp
strcmp("Tom", "Tim");        // direct call
(*Compare)("Tom", "Tim");    // indirect call
Compare("Tom", "Tim");       // indirect call, abbreviated
```  
  
A common use of a function pointer is to pass it as an argument to another function; typically because the latter requires different versions of the former in different circumstances. 
```cpp
int BinarySearch (char *item, char *table[], int n,
	              int (*Compare)(const char*, const char*))
{
  int bot = 0;
  int top = n - 1;
  int mid, cmp;

  while (bot <= top)
  {
  	mid = (bot + top) / 2;
  	if ((cmp = Compare(item, table[mid])) == 0)
  	  return mid;
  	else if (cmp < 0)
  	  top = mid - 1;
  	else
  	  bot = mid + 1;
  }
  return -1;
}
```
  
## References ##
A reference introduces an **alias** for an object. The notation for definition references is similar to that of pointers, except that & is used instead of \*. The following code defines num2 as a references to num1. After this definition *num1* and *num2* both refer to the same object, as if they were the same variable. A reference does not create a copy of an object, but merely a symbolic alias for it. After an assignment *num1 = 0.16*, both *num1* and *num2* will denote the value *0.16*.
```cpp
double num1  = 3.14;
double &num2 = num1; // num is a reference to num1

num1 = 0.16; // both num1 and num2 denote value 0.16
```
  
A reference must always be initialized when it is defined, it should be alias for something. It is illegal to define a reference and initialize it later. When initialize a reference to a constant, a copy of the constant value is created and the reference is set to refer to the copy.
```cpp
int &n = 1;  // n refers to a copy of 1
++n;
int y = n + 1; // y is 3 since n refers to a copy of 1, otherwise it would be 4 
```  
  
The most common use of references is for function parameters. Reference facilitates the **pass-by-reference** style of arguments, as opposed to the **pass-by-value** style.
  
```cpp
void Swap1 (int x, int y)  // pass-by-value (objects)
{
  int temp = x;
  x = y;
  y = temp;
}

void Swap2 (int *x, int *y) // pass-by-value (pointers)
{
  int temp = *x;
  *x = *y;
  *y = temp;
}

void Swap3 (int &x, int &y) // pass-by-reference
{
  int temp = x;
  x = y;
  y = temp;
}

int main (void)
{
  int i = 10, j = 20;
  Swap1(i, j);
  cout << i << ", " << j << '\n';

  Swap2(&i, &j);
  cout << i << ", " << j << '\n';

  Swap3(i, j);
  cout << i << ", " << j << '\n';
}
```  
The result of the above code is 
```console
10, 20
20, 10
10, 20
```
  
## Typedefs##
  
Typedef is a syntactic facility for introduction symbolic names for data types. Just as a reference defines an alias for an object, a typedef defines an alias for a type. Its main use is to simplify otherwise complicated type declarations as an aid to improved readability.

```cpp
typedef char *String;
typedef char Name[12];
typedef unsigned int uint;
```  
  
THe effect of these definitions is that *String* becomes an alias for *char\**, *Name* becomes an alias for an array of 12 *char*s, and *uint* is an alias for *unsigned int*. Therefore,
```cpp
String str;   // is the same as: char *str;
Name name;    // is the same as: char name[12];
uint n;       // is the same as: unsigned int n;
```  
  
The complicated declaration of *Compare* is a good candidate for typedef. The typedef introduces *Compare* as a new type name for any function with the given prototype. This makes *BinarySearch*'s signature arguably simpler.
```cpp
typedef int (*Compare)(const char*, const char*);

int BinarySearch (char *item, char *table[], int n, Compare comp)
{
  // ...
  if ((cmp = comp(item, table[mid])) == 0)
  	return mid;
}
```  
  
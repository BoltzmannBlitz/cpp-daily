---
layout: post
title:  "Expressions"
date:   2019-05-28 16:54:14
categories: c++
---
**Expressions** are evaluated to a certain value. Some expressions have **side-effects** when be evaluated. Side-effects are permanent changes in the program state.
## Arithmetric Operators
The following table shows arithmetric operators in C++.

| Operator names &nbsp;&nbsp;&nbsp; | Operator &nbsp;&nbsp;&nbsp; | syntax&nbsp;&nbsp;&nbsp; | Example |
| :-------------     | :------: | :-----: | :------------------ |
| unary plus         | +        | +a      | +1.63 // gives 1.63 |
| unary minus        | -        | -a      | -6.2  // gives -6.2 |
| addition           | +        | a + b   | 8 + 3.5  // = 11.5  |
| subtraction        | -        | a - b   | 8 - 3.5  // = 4.5   |
| multiplication     | \*       | a * b   | 3 * 3.5  // = 10.5  |
| division           | /        | a / b   | 8 / 2.5  // = 3.2   |
| modulo             | %        | a % b   | 8 % 3    // = 2     |
|  |  | | |                                        
|bitwise NOT         | ~        | ~a      | ~'\101' // gives '\276' |
|bitwise AND         | &        | a & b   | '\101' & '\110'  // gives '\100' | 
|bitwise OR          | \|       | a \| b  | '\101' \| '\110' // gives '\011' |
|bitwise XOR         | ^        | a ^ b   | '\101' ^ '\110'  // gives '\366' |
|bitwise left shift  | <<       | a << b  | '\101' << 2      // gives '\200' |
|bitwise right shift | >>       | a >> b  | '\101' >> 2      // gives '\006' |

&nbsp;

Note that
- integer division always results in an integer outcome (round down), e.g., 9/2=4, -9/2=-5
- **overflow** causes by a too large value for storing in a designed variable, the behaviour is undefined and machine-dependent.
- if divided by zero, it results a run-time *divided-by-zero* failure and causes the program to terminate.

How the bitwise operators calculated.

| Example &nbsp;&nbsp;&nbsp; | Octal Value &nbsp;&nbsp;&nbsp; | Bit Sequence|         
| :----- | :---------: | ------------: |                                        
| a      | 101         | 0100  0001    |
| b      | 110         | 0100  1000    | 
| ~ a    | 276         | 1011  1110    |
| a & b  | 100         | 0100  0000    |
| a \| b | 011         | 0000  1001    |
| a ^ b  | 366         | 1111  0110    |
| a >> 2 | 200         | 0001  0000    |
| a << 2 | 006         | 0000  0100    |

[**reference: arithmetric operators**](https://en.cppreference.com/w/cpp/language/operator_arithmetic)

## Comparison Operators
The following table shows comparison operators in C++.

| Operator names &nbsp;&nbsp;&nbsp; | Operator &nbsp;&nbsp;&nbsp; | syntax&nbsp;&nbsp;&nbsp; | Example |
| :-------------           | :------: | :-----: | :------------------ |
| euqal to                 | ==       | a == b  | 5 == 5 // gives 1 |
| not equal to             | !=       | a != b  | 3 != 3 // gives 0 |
| less than                | <        | a <  b  | 5 <  6 // gives 1 |
| less than or equal to    | <=       | a <= b  | 6 <= 5 // gives 0 |
| greater than             | >        | a >  b  | 6 >  5 // gives 1 |
| greater than or equal to | >=       | a >= b  | 5 >= 6 // gives 0 |

&nbsp;  
Note that string variables shold not be compared, because in this case the string *adresses* are compared not the contet. Strings can be compared by C++ library functions ***strcmp***.

[**reference： comparison operators**](https://en.cppreference.com/w/cpp/language/operator_comparison)

## Logical Operators
The following table shows logical operators in C++.

| Operator names &nbsp;&nbsp;&nbsp; | Operator &nbsp;&nbsp;&nbsp; | syntax&nbsp;&nbsp;&nbsp; | Example |
| :----------- | :------: | :-----: | :------------------ |
| negation     | !        | !a      | 5 == 5  // gives 1 |
| AND          | &&       | a && b  | 3 != 3  // gives 0 |
| inclusive OR | \|\|     | a \|\| b| 5 <  6  // gives 1 |

&nbsp;  
**Results**
&nbsp;  
!true = false  
!false = true  
&nbsp;  
true && true = true  
true && false = false  
false && false = false  
&nbsp;  
true \|\| true = true  
true \|\| false = true  
false \|\| false = false  

**Some examples**
```cpp  
!20 ;       // gives 0
15 && 2.5 ; // gives 1
0 && 2.5  ; // gives 0
14 || 5.5 ; // gives 1
```

[**reference： logical operators**](https://en.cppreference.com/w/cpp/language/operator_logical)

## Increment/Decrement Operators
The following table shows logical operators in C++. we define a variable: 
```cpp 
int a = 10;
```  

| Operator names &nbsp;&nbsp;&nbsp; | Operator &nbsp;&nbsp;&nbsp; | syntax&nbsp;&nbsp;&nbsp; | Example |
| :------------  | :---: | :-----: | :------------------ |
| pre-increment  | ++    | ++a  | ++a + 10  // gives 21 |
| post-decrement | ++    | a++  | a++ + 10  // gives 20 |
| pre-decrement  | --    | --a  | --a + 10  // gives 19 |
| post-decrement | --    | a--  | a-- + 10  // gives 20 |

## Assignment Operators
The assignment operator is used for storing a value at some memory location. Its left operand should be an *lvalue*, and its right operand is an arbitrary variable. The **lvalue** is anything that denotes a memory location to store a value. **lvalues** include variables, pointers, and references.

| Operator names &nbsp;&nbsp;&nbsp; | Operator &nbsp;&nbsp;&nbsp; | syntax&nbsp;&nbsp;&nbsp; | Example |
| :------------  | :---: | :-----: | :------------------ |
| simple assignment	              | =  | a = b   | a = 25 |
| addition assignment             | += | a += b  | a += 1 //equal to a=a + 1 |
| subtraction assignment          | -= | a -= b  | a -= 1 //equal to a=a - 1 |
| multiplication assignment       |\*= | a *= b  | a \*= 2 //equal to a=a \* 2 |
| division assignment             | /= | a /= b  | a /= 2  //equal to a=a / 2 |
| modulo assignment               | %  | a %= b  | a %= 2  //equal to a=a % 2 | 
| bitwise AND assignment          | &  | a &= b  | a &= 0xF22  //equal to a=a & 0xF22 |
| bitwise OR assignment           |\|= | a \|= b | a \|= 0xF22 //equal to a=a \| 0xF22 |
| bitwise XOR assignment          | ^= | a ^= b  | a ^= 0xF22  //equal to a=a ^ 0xF22 |
| bitwise left shift assignment   |<<= | a <<= b | a <<= 2     //equal to a=a << 2 |
| bitwise right shift assignment  |>>= | a >>= b | a >>= 2     //equal to a=a >> 2 |

Note  
An assignment operation is itself an expression whose value is the value stored in its left operand. Thus an assignment operation can therefore be used as the right operand of another assignment operation. Examples

```cpp
int m, n, p;
m = n = p = 100; // means (m=(n=(p=100)))
m = (n = p = 100) + 2; // means m=(n=(p=100))) + 2

m = 100;
m += n = p = 100; // means m = m + (n = (p = 100))
```

[**reference： assignment operators**](https://en.cppreference.com/w/cpp/language/operator_assignment)

## Conditional Operator
Conditional Operator ` ? : ` has the form *operand_1 ? operand_2 : operand_3*. First *operand_1* is evaluated and treated as a logical condition. If the result is nonzero then *operand_2* is evaluated as the final result. Otherwise, *operand_3* is evaluated and its value is the final result.

Example,  
```cpp
int m =3, n =2;
int min = (m > n) ? (n) : (m); // min == 2

//note the side-effect when using operators that change the program state
int min = (m < n) ? (m++) : (n++) // first evaluate (m<n), then evaluate n as result, finally n = n+1
```
  
## Comma Operator
Comma operator combines multiple expressions. The comma operator takes two operands. It first evaluates the left operand and then the right operand, and returns the value of the right operand as its final result.

```cpp
int m, n, min;
int m_count = 0, n_count = 0;
// m = ..., n = ...
min = ((m < n) ? (m_count++, m) : (n_count++, n));
```  
  
If *m* is greater than *n*, then *(n_count++, n)* is evaluated. It first evaluates *n_count*, then *n* and returns *n_count+1*.  

## ***sizeof*** Operator
In C++, ***sizeof*** operator is used to return the size of any data item or type in *byte*.

```cpp
#include <iostream>
using namespace std;
int main(void)
{
  cout << "size of char = " << sizeof(char) << "bytes\n";
  cout << "size of char* = " << sizeof(char*) << "bytes\n";
  cout << "size of short = " << sizeof(short) << "bytes\n";
  cout << "size of int = " << sizeof(int) << "bytes\n";
  cout << "size of long = " << sizeof(long) << "bytes\n";
  cout << "size of float = " << sizeof(float) << "bytes\n";
  cout << "size of double = " << sizeof(double) << "bytes\n";

  cout << "size of 1.5f = " << sizeof(1.5f) << "bytes\n";
  cout << "size of 1.5L = " << sizeof(1.5L) << "bytes\n";
  cout << "size of \"HELLO WORLD\" = " << sizeof("HELLO WORLD") << "bytes\n";
}
```  

This piece of code outputs the following result.  
```console
size of char = 1 bytes
size of char* = 8 bytes
size of short = 2 bytes
size of int = 4 bytes
size of long = 8 bytes
size of float = 4 bytes
size of double = 8 bytes
size of 1.5f = 4 bytes
size of 1.5L = 16 bytes
size of "HELLO WORLD" = 12 bytes
```  

## Operator Precedence
Precedence determines the order in which operators are evaluated in an expressions. The precedence rule represented by precedence levels. Operators in higher levels take precedence over operators in lower levels.

| Precedence &nbsp;&nbsp;&nbsp; | Operator &nbsp;&nbsp;&nbsp; | Associativity |
| ------------:  | :---: | :-----: |
|  1  |  ::  |  left-to-right  |
|  2  |  a++, a--, type(), type{}, a(), a[]  |  left-to-right  |
|  3  |  ++a, --a, +a, -a, !a, ~a, &a, sizeof, new, new[], delete, delete[]   |  right-to-left  |
|  4  |  .\*, ->\*  |  left-to-right  |
|  5  |  a*b, a/b, a%b  |  left-to-right  |
|  6  |  a+b, a-b  |  left-to-right  |
|  7  |  <<, >>  |  left-to-right  |
|  8  |  <=> (c++20)  |  left-to-right  |
|  9  |  <, <=, >, >=  |  left-to-right  |
| 10  |  ==, !=  |  left-to-right  |
| 11  |  &   |  left-to-right  |
| 12  |  ^   |  left-to-right  |
| 13  |  \|  |  left-to-right  |
| 14  |  &&  |  left-to-right  |
| 15  |  \|\||  left-to-right  |
| 16  |  a?b:c, throw, =, +=, -=, *=, /=, %=, <<=, >>=, &=, ^=, \|=  |  right-to-left  |
| 17  |  ,  |  left-to-right  |
  
[**reference： operator precedence**](https://en.cppreference.com/w/cpp/language/operator_precedence)
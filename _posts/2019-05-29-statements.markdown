---
layout: post
title:  "Statements"
date:   2019-05-29 19:10:14
categories: c++
---

##Simple and Compound Statements  
  
A **simple** statement is a computation terminated by a semicolon.

```cpp
int i;             // declaration statement
++i;               // this has a side-effect
double d = 10.05;  // declaration statement
d+5;               // useless statement!
;                  // null statement
```  
  
Multiple statements can be combined into a **compound** statement by enclosing them within braces (also called as a **block**). Compound statements 1). allow us to put multiple statements in places where otherwise only single statements are allowed, and 2). allow us to introduce new **scope** in the program. Variables that are defined in a scope lives within the scope.
  
```cpp
{
  int min, i = 10, j = 20;
  min = (i < j) ? i : j;
  cout << min << '\n';
}
```  
  
## The if Statement
If statement provides a mechanism to execute a statement (or block) dependent upon a condition being satisfied. The general form of if statements is:  

if (*expression*)  
 &nbsp;&nbsp;*statement*;  
or  
if (*expression*)  
{  
 &nbsp;&nbsp;*statement_1*;  
 &nbsp;&nbsp;*statement_2*;  
}   
  
Examples,
```cpp
if (count != 0)
  average = sum / count;
```
  
```cpp
if (balance > 0)
{
  interest = balance * creditRate;
  balance += interest;
}
```  
  
*if-else* is a variant version of *if* statement. It specifies two alternative statements: one which is executed if a condition is satisfied and one which is executed if the condition is not satisfied. The following gives a general form of *if-else* statement.
  
if (*expression*)  
 &nbsp;&nbsp;*statement_1*;   
 else  
 &nbsp;&nbsp;*statement_2*;  

Example, 
```cpp
// this piece of code calculates the area of two polygons
  if ((intersected= polygon_intersection(query_polygon, target_polygon))!=null)
  {
    return area(query_polygon) + area(target_polygon) - area(intersected);
  }
  else
  {
    return area(query_polygon) + area(target_polygon);
  }
```
  
Sometimes *if-else* may be simplified by using conditional expression.
  
```cpp 
((intersected= polygon_intersection(query_polygon, target_polygon))!=null) ? (area(query_polygon) + area(target_polygon) - area(polygon_intersection(query_polygon, target_polygon))) : area(query_polygon) + area(target_polygon);
```
  
*if* statements may be nested, for example  

if (*expression_1*)  
{  
&nbsp;&nbsp;if (*expression_2*)  
&nbsp;&nbsp;&nbsp;&nbsp;*statement_1*;  
&nbsp;&nbsp;else  
&nbsp;&nbsp;{  
&nbsp;&nbsp;&nbsp;&nbsp;*statement_2*;  
&nbsp;&nbsp;&nbsp;&nbsp;*statement_3*;  
&nbsp;&nbsp;}  
}  
else  
&nbsp;&nbsp;*statement_4*;  
  
and  
  
if (*expression_1*)  
&nbsp;&nbsp;*statement_1*;  
else if (*expression_2*)   
&nbsp;&nbsp;*statement_2*;  
else if (*expression_3*)  
&nbsp;&nbsp;if (*expression_4*)  
&nbsp;&nbsp;&nbsp;&nbsp;*statement_3*;  
&nbsp;&nbsp;else  
&nbsp;&nbsp;&nbsp;&nbsp;*statement_4*;  
else  
&nbsp;&nbsp;*statement_5*;  

## The switch Statement  
The *switch* statement provides a way of choosing between a set of alternatives, based on the value of an expression. The general form of the switch statement is,  
  
switch (*expression*)  
{  
&nbsp;&nbsp;case *constant_1*:  
&nbsp;&nbsp;&nbsp;&nbsp;*statement*;  
...   
&nbsp;&nbsp;case *constant_2*:  
&nbsp;&nbsp;&nbsp;&nbsp;*statement*;  
&nbsp;&nbsp;default:  
&nbsp;&nbsp;&nbsp;&nbsp;*statement*;  
}  
  
At first, the expression (**switch tag**) is evaluated, and its result is compared to each of the numeric constant (**label**) in **case**, until a match is found. Then the *statement* in the matching case are executed. The **default** case is optional, and is executed if none of its above cases provide a match. The execution continues if either a **break** statement is encountered or all intervening statements until the end of the switch statement are executed.
  
```cpp
switch(TIME_ZONE)                        // TIME_ZONE is defined as a int
  case ZONE_UTC:
    local_time = current_utc_time;
  case ZONE_UTC_PLUS_1:                  // ZONE_UTC_PLUS_1 = 1        
    local_time = current_utc_time + 1.;
  case ZONE_UTC_PLUS_2:                  // ZONE_UTC_PLUS_2 = 2 
    local_time = current_utc_time + 2.;
  case ZONE_UTC_MINUS_1:                 // ZONE_UTC_MINUS_1 = -1
    local_time = current_utc_time - 1.;
  // ZONE_UTC_ continues here
  default:
    local_time = -1000; // undefined time zone
```
  
Note that though the *switch* statements may be rewritten with a set of *if-else* statements, perference should be given to the switch version when possible. Because it is much neater, unless the conditions involved are not simply equality expressions, or labels are not numeric constants.  
  
##The Loop Statements
The Loop Statements provide ways of repeating an statement while a condition holds. C++ provides three flavors of iteration. 
###The *while* statement
The *while* statement (also called **while loop**) has the following general form,
  
while(*expression*)  
&nbsp;&nbsp;statement;  
  
The *expression* (**loop condition**) is first evaluated and if its result is nonzero then the *statement* (**loop body**) is executed. Then the whole process is repeated until the expression gives out a zero.  
The following example calculates the sum of all numbers from *1* to *n*,  
```cpp
int i= 1;
int sum=0;
while (i <= n)
{
  sum=+ i++;
}
```
  
The while loop may have an empty body (i.e., a null statement). The following loop sets *n* to its greatest odd factor.
```cpp
while ( n% 2== 0 && n /= 2)
  ;
```
  
###The do-while Statement
The *do-while* statement (also called **do loop**) is similar to the *while* statement, except that its body is executed first and then the loop condition is examined. The general form of the *do* statement is,
  
do  
&nbsp;&nbsp;*statement*;  
while (*expression*);  
  
The formal calculation of the sum of all numbers from *1* to *n* can be rewritten as,
```cpp
int i= 1;
int sum=0;

do
{
  sum=+ i++;
}
while (i <= n+1)
```
  
Though a do loop with a null body would be equivalent to a similar while loop, the latter is always preferred for its superior readability.  
  
###The for Statement
The *for* statement (**for loop**) is similar to the *while* statement, but has two additional components, an expression which is evaluated only once before everything else, and an expression which is evaluated once at the end of each iteration. It has the following general form,  
  
  
for (*expression_1*, *expression_2*, *expression_3*)  
{  
&nbsp;&nbsp;statement;  
}  
  
First *expression_1* is evaluated. In each iteration of the loop *expression_2* is evaluated as a termination condition. If its outcome is nonzero then statement *expression_3* is evaluated, otherwise the loop is terminated.  The general for loop is equivalent to the following while loop,  
  
*expression_1*;  
while (*expression_2*)  
{  
  *statement*;  
  *expression_3*;  
}  
  
The most common use of *for* loops is for situations where a variable is incremented or decremented with every iteration of the loop.
  
```cpp
const int max_iterations = 100;
int sum = 0;
for (int i = 1; i <= max_iterations; ++i)   // i is called the loop variable
  sum += i;
```
  
The scope for *i* is not the body of the loop, but the loop itself. Scope-wise, the above is equivalent to,
  
```cpp
int i;
for (i = 1; i <= max_iterations; ++i)   // i is called the loop variable
  sum += i;
```  
  
Some possible *for*-loops,  

for (&nbsp;; &nbsp;i != 0; &nbsp;)  // is equivalent to: while (i ！= 0)
{  
&nbsp;&nbsp;something;  
}  
  
for (&nbsp;; &nbsp;; &nbsp;)  // infinite loop
{  
&nbsp;&nbsp;something;  
}  
  
Example of a loop with multiple loop variable.  
  
for (i = 0, j = 0; i + j < n; ++i, ++j)  
&nbsp;&nbsp;something;  
  
Example of a nested loop.  
  
for (i = 0; i < n; ++i)  
&nbsp;&nbsp;for (j = 0; j < m; ++j)  
&nbsp;&nbsp;&nbsp;&nbsp;something;  
  
##The continue Statement
The *continue* statement terminates the current iteration of a loop and instead jumps to the next iteration. Examples are given here,  
  
do  
{  
&nbsp;&nbsp;cin >> num;  
&nbsp;&nbsp;if (num < 0)  
&nbsp;&nbsp;&nbsp;&nbsp;continue; // process num here...  
}  
while (num != 0);  
  
do {  
&nbsp;&nbsp;cin >> num;  
&nbsp;&nbsp;if (num >= 0)  
&nbsp;&nbsp;{  
&nbsp;&nbsp;&nbsp;&nbsp;// process num here...   
&nbsp;&nbsp;}   
} while (num != 0);  
  
##The break Statement
A *break* statement may appear inside a loop (*while*, *do-while*, or *for*) or a *switch* statement. The *break* statement will ignores a *if* statement.  
  
for (i = 0; i < attempts; ++i)   
{   
&nbsp;&nbsp;cout << "Please enter your password: ";   
&nbsp;&nbsp;cin >> password;   
&nbsp;&nbsp;if (Verify(password)) // check password for correctness   
&nbsp;&nbsp;&nbsp;&nbsp;break; // drop out of the loop   
&nbsp;&nbsp;cout << "Incorrect!\n";   
}  
  
The break version is simpler and preferred, while it can be rewritten as  
  
for (i = 0; i < attempts; ++i)   
{   
&nbsp;&nbsp;cout << "Please enter your password: ";   
&nbsp;&nbsp;cin >> password;   
&nbsp;&nbsp;verified = Verify(password) // check password for correctness   
&nbsp;&nbsp;if (!verified)
&nbsp;&nbsp;&nbsp;&nbsp;cout << "Incorrect!\n";   
}  
  
##The goto Statement
The *goto* statement provides the lowest-level of jumping. It has the general form,

goto *label*;  
  
where *label* is an identifier which marks the jump destination of *goto*. The label should be followed by a colon and appear before a statement within the same function as the goto statement itself. The following example rewrites the break example.
  
for (i = 0; i < attempts; ++i)   
{   
&nbsp;&nbsp;cout << "Please enter your password: ";   
&nbsp;&nbsp;cin >> password;   
&nbsp;&nbsp;if (Verify(password)) // check password for correctness   
&nbsp;&nbsp;&nbsp;&nbsp;goto out; // drop out of the loop   
&nbsp;&nbsp;cout << "Incorrect!\n";   
}  
out:  
  //something...  
  


